require 'rubygems'
require 'sinatra/base'
require 'tempfile'

class Webboole < Sinatra::Base
  # Helper Methods
  VALID = 0;
  SATISFY = 1;

  def limboole code,switch
    file = Tempfile.new("limboole")
    file.write(code)
    file.flush

    if switch.to_i == VALID
      result = %x(limboole #{file.path})
    elsif switch.to_i == SATISFY
      result = %x(limboole -s #{file.path})
    end
    file.close
    result
  end

  # Action Methods
  get '/' do
    erb :index
  end

  post '/evaluate' do
    limboole(params[:postData],params[:switch])
  end
end
